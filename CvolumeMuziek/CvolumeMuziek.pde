import processing.sound.*;
SoundFile file;

void setup() {
  size(640, 360);
  background(255);
    
  file = new SoundFile(this, "sax.mp3");
  file.play();
  file.rate(1);
}      

void keyPressed() {
    if (key == '1') {
        file.amp(0.3);
    }
    if (key == '2') {
        file.amp(0.7);
    }
    if (key == '3') {
        file.amp(1.0);
    }
    if (key == '4') {
        file.stop();
    }
    if (key == '5') {
        file.play();
    }
  }

void draw() {

}
