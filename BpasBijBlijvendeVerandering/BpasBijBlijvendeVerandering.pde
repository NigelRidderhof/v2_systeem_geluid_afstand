import processing.video.*;

Capture video = null;

color trackColor; 
float threshold = 300;
float distThreshold = 15;

ArrayList<Blob> blobs = new ArrayList<Blob>();

int hvlMomenten = 75;
int[] blobMomenten = new int[hvlMomenten];
String blobStatus;

void setup() {
  size(640, 480);
  String[] cameras = Capture.list();
  printArray(cameras);
  video = null;
  video = new Capture(this, Capture.list()[135]);
  video.start();
  trackColor = color(0, 0, 0);
}

void captureEvent(Capture video) {
  video.read();
}

void keyPressed() {
  if (key == 'a') {
    distThreshold+=5;
  } else if (key == 'z') {
    distThreshold-=5;
  }
  if (key == 's') {
    threshold+=5;
  } else if (key == 'x') {
    threshold-=5;
  }


  println("Distance threshold:" + distThreshold);
  println("Color threshold:" + threshold);
}

void draw() {
  video.loadPixels();
  image(video, 0, 0);

  blobs.clear();


  // Begin loop to walk through every pixel
  for (int x = 0; x < video.width; x++ ) {
    for (int y = 0; y < video.height; y++ ) {
      int loc = x + y * video.width;
      // What is current color
      color currentColor = video.pixels[loc];
      float r1 = red(currentColor);
      float g1 = green(currentColor);
      float b1 = blue(currentColor);
      float r2 = red(trackColor);
      float g2 = green(trackColor);
      float b2 = blue(trackColor);

      float d = distSq(r1, g1, b1, r2, g2, b2); 

      if (d > threshold*threshold) {

        boolean found = false;
        for (Blob b : blobs) {
          if (b.isNear(x, y)) {
            b.add(x, y);
            found = true;
            break;
          }
        }

        if (!found) {
          Blob b = new Blob(x, y);
          blobs.add(b);
        }
      }
    }
  }

  int amountBlobs = 0;

  for (Blob b : blobs) {
    if (b.size() > 500) {
      b.show();
      
      amountBlobs++;
    }
  }
  
  blobMomenten[0] = amountBlobs;
  for (int x = hvlMomenten-1; x > 0; x--){
      blobMomenten[x] = blobMomenten[x-1];
  }
 
  println(amountBlobs + "and" + blobMomenten[hvlMomenten-1] + "and" + blobMomenten[0] + "and" + blobMomenten[1]);
  
  if(alleMomentenHetzelfde(blobMomenten) == true){
                if(blobMomenten[0] == 1){
                    text("uno", width-10, 150);
                    blobStatus = "een";
                } else if (blobMomenten[0] == 2) {
                    text("2 blobs", width-10, 125);
                    blobStatus = "twee";
                } else {
                    text("meer dan 2 of 0 blobs", width-10, 150);
                    blobStatus = "rest";
                }
  }
  
  switch(blobStatus) {
   case "een" :
      //eerst kijken of groter is dan gemiddelde blob grootte die in eerder is vastgesteld Zoja, invloed van kortste afstand activeren. Zonee, geluid uit zetten.
      break;
   
   case "twee" :
       //als nog niet speelt aanzetten, anders afstand meten en de invloed hoeveelheid aanpassen
      break;
   
   default :
       //geluid uit zetten 
}
  
// cases alles1(als groter dan gemiddeld, exra geluid, anders geen geluid), alles2 (afstand meten en geluid beinvloeden), geen geluid
//voor t gemiddelde moet in t begin ff grootte steeds bijgehouden worden en berekend

  textAlign(RIGHT);
  fill(0);
  text("distance threshold: " + distThreshold, width-10, 25);
  text("color threshold: " + threshold, width-10, 50);
}


float distSq(float x1, float y1, float x2, float y2) {
  float d = (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1);
  return d;
}


float distSq(float x1, float y1, float z1, float x2, float y2, float z2) {
  float d = (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) +(z2-z1)*(z2-z1);
  return d;
}

void mousePressed() {
  // Save color where the mouse is clicked in trackColor variable
  int loc = mouseX + mouseY*video.width;
  trackColor = video.pixels[loc];
}

boolean alleMomentenHetzelfde(int[] deMomenten){
    for (int x = 0; x < hvlMomenten-1; x++){
        if(deMomenten[x] != deMomenten[x+1]){
            return false;
        } 
    }
    return true;
}
